from esky import bdist_esky
from distutils.core import setup
from cx_Freeze import setup, Executable

setup(name="appveyortest",
      version="0.0.1",
      scripts=["scripts/appveyortest.py"],
      executables=[Executable("scripts/appveyortest.py")],
      options={
          "bdist_esky": {
              "includes": ["appveyortest"]
          },
          "build_exe": {
          }
      })
