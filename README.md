# appveyor-test

Testing out appveyor, a continuous integration environment similar to Travis, but focused on Windows environments.

In particular, we want to make sure we can continuously build artifacts that include:

- PyQt4 / PyQt5
- Python 3.4
- frozen artifacts thereof, e.g. via esky or pyqtdeploy (or both)
